<?php
namespace Drupal\hello_world\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ExampleForm extends FormBase {
	public function getFormId() {
		return 'hello_world_example_form';
}

	public function buildForm(array $form, FormStateInterface $form_state) {
		$form['company_name'] = array(
			'#type' => 'textfield',
			'#title' => $this->t('Company name'),
		);
		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => $this->t('Save'),
		);
		return $form;
}
	public function validateForm(array &$form, FormStateInterface $form_state) {
	}

	public function submitForm(array &$form, FormStateInterface $form_state) {
	}
}