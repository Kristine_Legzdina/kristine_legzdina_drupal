Drupal8 W3CSS Theme is using the W3.CSS framework with built-in responsiveness:
- Smaller and faster than other CSS frameworks.
- Easier to learn, and easier to use than other CSS frameworks.
- Uses standard CSS only (No jQuery or JavaScript library).
- Speeds up and simplifies web development.
- Supports modern responsive design (mobile first) by default.
- Provides CSS equality for all browsers. Chrome, Firefox, IE, Safari, and more.
- Provides CSS equality for all devices. PC, laptop, tablet, and mobile.
- Learn more about W3.CSS https://www.w3schools.com/w3css/


========================================================
|=           DRUPAL8 W3CSS THEME FEATURES             =|
========================================================
1-Drupal8 W3CSS Theme is using the W3.CSS framework with built-in responsiveness
2- Drupal8 W3CSS Theme has 22 regions.
- Regions Will show on ALL pages: Header, Primary Menu Vertical, Primary Menu,
Highlighted, Breadcrumb, Content, Left Sidebar, Right Sidebar, Footer First
Container, Footer Second Container, Footer Third Container and Footer Menu
Container.
- Regions will show on All pages EXCEPT the front page: Page Title.
- Regions will show ONLY on the front page: Welcome Text, Top First container,
Top Second Container, Top Third Container, Bottom First Container, Bottom Second
Container, Bottom Third Container and Bottom Forth Container.
3- Drupal8 W3CSS Theme has 26 sections with changeable colors. With ww3.css
frameworks you can change the background color, text color, text shadow and more
 to any section in your website.  Just simply apply the w3.css class to the
 section.
4- Drupal8 W3CSS Theme comes with ability to change the site width. Just apply
any of these per-configured classes to your website. (w3-width-100-percent,
w3-width-2560, w3-width-1920, w3-width-1600, w3-width-1360, w3-width-1280,
w3-width-1024 and w3-width-800).
5- Drupal8 W3CSS Theme comes with horizontal main menu or vertical side nav.
Simply add the main menu to the horizontal region or the vertical region.
6- Drupal8 W3CSS Theme comes with 22 predefined w3css theme.  Simply insert the
name of any of the predefined theme.
7- Drupal8 W3CSS Theme comes with ability to create your own private w3.css
theme and apply it to your site.
8- Drupal8 W3CSS Theme comes with 8 social media links easy to configure.
9- Drupal8 W3CSS Theme has a top region with three flexible blocks. Block width
will change automatically based on the number of assigned blocks. Block share
equal width based on number of assigned blocks. (Example, If you have only two
blocks in top area block, Block share 50% each block, If you assign three it
come 33.3% each). Block width will change automatically based on assigned block,
Block share equal width based on assigned block. (Example, If you have only two
blocks in top area block, Block share 50% each block, If you assign three it
come 33.3% each).
10- Drupal8 W3CSS Theme has a bottom region with four flexible blocks. Block
width will change automatically based on the number of assigned blocks. Block
share equal width based on number of assigned blocks. (Example, If you have only
three blocks in bottom region, Block share 33.3% each block, If you assign four
it come 25% each).
11- Drupal8 W3CSS Theme has a footer region with three flexible blocks. Block
width will change automatically based on the number of assigned blocks. Block
share equal width based on number of assigned blocks. (Example, If you have only
two blocks in footer region, Block share 50% each block, If you assign three it
come 33.3% each).
12- Drupal8 W3CSS Theme comes with font awesome added to the css library.
13- Drupal8 W3CSS Theme comes with jQuery match height. Example, if you have
three blocks in the top area, they will always have the height in any width.
14- Drupal8 W3CSS Theme comes responsive main menu and a drop-down menu.
The drop-down works on hover for big screen and on click for small screen.
15- Drupal8 W3CSS Theme will hide completely (divs, wrappers, classes) any
region you don't use.  Nothing will be printed in the back.
16- Drupal8 W3CSS Theme uses the minimum HTML structure to show the content.
Example, You will not see div > div > div > div > div.
17- Drupal8 W3CSS Theme comes with the ability to create a custom page template
according to content type's name.  If you have two content types article and
video you can have page--article.html.twig and page--video.html.twig
18- Drupal8 W3CSS Theme comes with the ability to create a custom page template
for any view.  Example if you have a view name "recent_video", then you can
create a page template name page--recent-video.html.twig.
You can view live demo by going to:
<a href="http://drupal8.w3css.theme.flashwebcenter.com" title="Drupal8 W3CSS
Theme Demo">http://drupal8.w3css.theme.flashwebcenter.com/</a>
========================================================
|=       DRUPAL8 W3CSS THEME CONFIGURATION            =|
========================================================

|-  How to install drupal8 w3css theme
==--+-+--+--+--+-+--+--+--+--+-+--+--+--+-+--+--+--+--==
Download the theme to www.your-drupal-site-name/themes/ and go to
your-site-domain/admin/appearance and scroll to the bottom until you see Drupal8
W3CSS Theme and click on Install and set as default and click save.

|-  How to change the logo
==--+-+--+--+--+-+--+--+--+--+-+--+--+--+-+--+--+--+--==
Go to www.your-drupal-site-name/admin/appearance/settings/drupal8_w3css_theme
and uncheck “Use the logo supplied by the theme” then upload your own website
logo and click save.

|-  How to change the favicon
==--+-+--+--+--+-+--+--+--+--+-+--+--+--+-+--+--+--+--==
Go to www.your-drupal-site-name/admin/appearance/settings/drupal8_w3css_theme
and uncheck “Use the favicon supplied by the theme” then upload your own favicon
and click save.

|-  How to change website width
==--+-+--+--+--+-+--+--+--+--+-+--+--+--+-+--+--+--+--==
Go to www.your-drupal-site-name/admin/appearance/settings/drupal8_w3css_theme
and click on Website Width to expand it. Copy and paste your desired website
width and click save.

|-  How to use the w3css predefined themes
==--+-+--+--+--+-+--+--+--+--+-+--+--+--+-+--+--+--+--==
Go to www.your-drupal-site-name/admin/appearance/settings/drupal8_w3css_theme
and click on  w3css predefined themes to expand it.  Copy and paste any of your
desired w3css color theme and click save.

|-  How to change website colors
==--+-+--+--+--+-+--+--+--+--+-+--+--+--+-+--+--+--+--==
Go to www.your-drupal-site-name/admin/appearance/settings/drupal8_w3css_theme
and click on Advanced Site Colors to expand it.  Each section in the website can
be modified and click save.  You can change the text color, background color,
padding, margin or borders.  You can use any of W3 CSS Classes
https://www.w3schools.com/w3css/w3css_references.asp

|-  How to change Social Media Links
==--+-+--+--+--+-+--+--+--+--+-+--+--+--+-+--+--+--+--==
Go to www.your-drupal-site-name/admin/appearance/settings/drupal8_w3css_theme
and click on Social Media Link to expand it.  Change any of the links and click
save.

|-  How to change copyrighT
==--+-+--+--+--+-+--+--+--+--+-+--+--+--+-+--+--+--+--==
Go to www.your-drupal-site-name/admin/appearance/settings/drupal8_w3css_theme
and click on Copyright to expand it, then change the text there and click save.

|-  How to disable the credit at the bottom
==--+-+--+--+--+-+--+--+--+--+-+--+--+--+-+--+--+--+--==
Go to www.your-drupal-site-name/admin/appearance/settings/drupal8_w3css_theme
and click on Credit to expand it.  Uncheck the Show/Hide Credit Text and click
save.

|-  How to create a new custom predefined color theme
==--+-+--+--+--+-+--+--+--+--+-+--+--+--+-+--+--+--+--==
1- Go to https://www.w3schools.com/w3css/w3css_color_themes.asp and create your
custom theme.  Follow the steps to create a private theme.
2- After you finish go to your-drupal-site-directory/themes/drupal8_w3css_theme/
css/w3-css-theme-custom/w3-theme-custom.css Open w3-theme-custom.css and delete
the old code then paste the new code for your private theme.
3- Go to www.your-drupal-site-name/admin/appearance/settings/drupal8_w3css_theme
and click on W3css Predefined Theme and enter w3-theme-custom. How to install
drupal8 w3css subtheme >>  Install Drupal8 W3CSS Theme First, then install
drupal8 w3css subtheme.

|-  How to use the drupal8_w3css_subtheme
==--+-+--+--+--+-+--+--+--+--+-+--+--+--+-+--+--+--+--==
Inside your-drupal-site-directory/themes/drupal8_w3css_theme/
drupal8_w3css_subtheme you will have all the necessary files for start up theme.
